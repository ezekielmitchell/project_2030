import time
from datetime import datetime
import threading
from envirophat import light, weather
import csv
import matplotlib.pyplot as plt
import numpy as np

# global variables
stopRun = False
now = None
lighting = None
temp = None
startTime = time.perf_counter()

# data collection
dataFile = 'envirophat_data.csv'
fields = ['Date/time', 'Temperature', 'Lighting']

def writing():
	with open(dataFile, mode='w', newline='') as file:
		writer = csv.writer(file)
		#writer.writerow(['Date-Time', 'Temperature', 'Lighting'])
		


# collect date/time data
def dateTime():
	global now 
	while not stopRun:
		currentTime = datetime.now().strftime('%H:%M:%S')
		now = currentTime
		print(f'Time: {now}')
		time.sleep(3)



# collect light data
def lightData():
	global lighting
	while not stopRun: 
		currentLight = light.light()
		lighting = currentLight
		print(f'Light level: {lighting}')
		time.sleep(3)
	

# collect temperature data
def tempData():
	global temp
	while not stopRun: 
		currentTemp = weather.temperature()
		temp = round(currentTemp, 2)
		print(f'Temperature: {temp}')
		time.sleep(3)
		
# push data to csv
def dataCollect():
	with open(dataFile, mode='w', newline='') as file:
		writer=csv.writer(file)
		while not stopRun:
			if now is not None and temp is not None and lighting is not None:
				writer.writerow([now, temp, lighting])
			time.sleep(3)


# plot data
def plotData():
    data = np.genfromtxt(dataFile, delimiter=',', names=['Time', 'Temperature', 'Light'], dtype=None)
    plt.plot(data['Time'], data['Temperature'], label='Temperature')
    plt.xlabel('Time')
    plt.ylabel('Temperature')
    plt.legend()
    plt.show()

if __name__ == "__main__":
	
	#global stopRun
	#writing()
	
	
	try:
		# thread creation
		timeThread = threading.Thread(target=dateTime)
		tempThread = threading.Thread(target=tempData)
		lightThread = threading.Thread(target=lightData)
		dataThread = threading.Thread(target=dataCollect)
		
		# run threads
		timeThread.start()
		tempThread.start()
		lightThread.start()
		dataThread.start()
		

		while not stopRun:
			time.sleep(1)
			
	except KeyboardInterrupt:
	
		print()
		print("Exiting...")
		stopRun = True
		time.sleep(3.1)

	except Exception as e:
		print('failed at execution: ' + e)
	
	finally:
		# to view the plot -- press Cmd (+) C after desired period of time
		plotData()

		print('trial complete')
	

